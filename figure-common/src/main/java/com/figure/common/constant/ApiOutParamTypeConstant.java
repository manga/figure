package com.figure.common.constant;

/**
 * Created by chuanbo.wei on 2/3/2016.
 */
public enum ApiOutParamTypeConstant {
    //元数据类别定义 1：基本类型，2：复杂类型，3：列表类型，4：数组类型，5：枚举类型，6：文件类型
    BASE("基本类型",1),
    STRUCT("复杂类型",2),
    LIST("列表类型",3),
    ARRAY("数组类型",4),
    ENUM("枚举类型",5),
    FILE("文件类型",6);

    private String name;
    private int value;

    ApiOutParamTypeConstant(String name, int value){
        this.name = name;
        this.value = value;
    }

    public static ApiOutParamTypeConstant get(String name){
        for(ApiOutParamTypeConstant constant : ApiOutParamTypeConstant.values()){
            if(constant.getName().equals(name)){
                return constant;
            }
        }
        return null;
    }

    public static ApiOutParamTypeConstant get(int value){
        for(ApiOutParamTypeConstant constant : ApiOutParamTypeConstant.values()){
            if(constant.getValue()==value){
                return constant;
            }
        }
        return null;
    }

    public static boolean isExists(int value){
        for(ApiOutParamTypeConstant constant : ApiOutParamTypeConstant.values()){
            if(constant.getValue()==value){
                return true;
            }
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isList() {
        return value == LIST.value || value == ARRAY.value;
    }

    public boolean isStruct() {
        return value == STRUCT.value || isList();
    }
}
