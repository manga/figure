package com.figure.core.dao.item;

import com.figure.core.model.item.FigureItem;
import com.figure.core.model.item.FigureItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FigureItemMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int countByExample(FigureItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int deleteByExample(FigureItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int deleteByPrimaryKey(String openIid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int insert(FigureItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int insertSelective(FigureItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    List<FigureItem> selectByExample(FigureItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    FigureItem selectByPrimaryKey(String openIid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByExampleSelective(@Param("record") FigureItem record, @Param("example") FigureItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByExample(@Param("record") FigureItem record, @Param("example") FigureItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByPrimaryKeySelective(FigureItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table figure_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByPrimaryKey(FigureItem record);

    void insertBatch(List<FigureItem> list);

    List<FigureItem> selectByCondition(@Param("title") String title, @Param("orderType")String orderType,@Param("categoryId") Long categoryId);

    List<FigureItem> selectItemByCondition(@Param("title") String title, @Param("orderType")String orderType);
}