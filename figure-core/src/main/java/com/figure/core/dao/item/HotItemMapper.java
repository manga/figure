package com.figure.core.dao.item;

import com.figure.core.model.item.HotItem;
import com.figure.core.model.item.HotItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HotItemMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int countByExample(HotItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int deleteByExample(HotItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int deleteByPrimaryKey(String openIid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int insert(HotItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int insertSelective(HotItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    List<HotItem> selectByExample(HotItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    HotItem selectByPrimaryKey(String openIid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByExampleSelective(@Param("record") HotItem record, @Param("example") HotItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByExample(@Param("record") HotItem record, @Param("example") HotItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByPrimaryKeySelective(HotItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table hot_item
     *
     * @mbggenerated Thu Sep 08 11:02:47 CST 2016
     */
    int updateByPrimaryKey(HotItem record);

    void insertBatch(List<HotItem> list);
}