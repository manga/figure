package com.figure.core.dao.security;

import com.figure.core.model.security.AccountRole;
import com.figure.core.model.security.AccountRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AccountRoleMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int countByExample(AccountRoleExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int deleteByExample(AccountRoleExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int insert(AccountRole record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int insertSelective(AccountRole record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    List<AccountRole> selectByExample(AccountRoleExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    AccountRole selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByExampleSelective(@Param("record") AccountRole record, @Param("example") AccountRoleExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByExample(@Param("record") AccountRole record, @Param("example") AccountRoleExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByPrimaryKeySelective(AccountRole record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table account_role
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByPrimaryKey(AccountRole record);
}