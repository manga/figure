package com.figure.core.dao.security;

import com.figure.core.model.security.Resources;
import com.figure.core.model.security.ResourcesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ResourcesMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int countByExample(ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int deleteByExample(ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int insert(Resources record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int insertSelective(Resources record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    List<Resources> selectByExampleWithBLOBs(ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    List<Resources> selectByExample(ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    Resources selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByExampleSelective(@Param("record") Resources record, @Param("example") ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByExampleWithBLOBs(@Param("record") Resources record, @Param("example") ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByExample(@Param("record") Resources record, @Param("example") ResourcesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByPrimaryKeySelective(Resources record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByPrimaryKeyWithBLOBs(Resources record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table resources
     *
     * @mbggenerated Sun Sep 04 22:35:10 CST 2016
     */
    int updateByPrimaryKey(Resources record);

    /**
     *
     * @param accountId
     * @return
     */
    List<Resources> findUserResourcess(Long accountId);
}