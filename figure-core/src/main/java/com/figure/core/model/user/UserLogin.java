package com.figure.core.model.user;

import java.util.Date;

public class UserLogin {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.id
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    private Long id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.user_id
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    private String userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.login_time
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    private Date loginTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column user_login.login_ip
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    private String loginIp;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.id
     *
     * @return the value of user_login.id
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.id
     *
     * @param id the value for user_login.id
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.user_id
     *
     * @return the value of user_login.user_id
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public String getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.user_id
     *
     * @param userId the value for user_login.user_id
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.login_time
     *
     * @return the value of user_login.login_time
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public Date getLoginTime() {
        return loginTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.login_time
     *
     * @param loginTime the value for user_login.login_time
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column user_login.login_ip
     *
     * @return the value of user_login.login_ip
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public String getLoginIp() {
        return loginIp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column user_login.login_ip
     *
     * @param loginIp the value for user_login.login_ip
     *
     * @mbggenerated Thu Sep 01 22:03:06 CST 2016
     */
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }
}