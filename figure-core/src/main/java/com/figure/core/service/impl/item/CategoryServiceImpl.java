package com.figure.core.service.impl.item;

import com.figure.core.dao.item.CategoryMapper;
import com.figure.core.model.item.Category;
import com.figure.core.model.item.CategoryExample;
import com.figure.service.constant.CategoryIsHotConstant;
import com.figure.service.constant.StatusConstant;
import com.figure.service.dto.item.CategoryDto;
import com.figure.service.provide.item.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chuanbo.wei on 2016/9/1.
 */
@Service("categoryService")
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public List<CategoryDto> getCategoryList() throws Exception {
        CategoryExample example = new CategoryExample();
//        example.createCriteria().andStatusEqualTo(StatusConstant.OK.getValue());
        CategoryExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(StatusConstant.OK.getValue());
        criteria.andTypeEqualTo("0");
        List<Category> list = categoryMapper.selectByExample(example);
        if(null==list || list.size()<=0)
            return null;
        List<CategoryDto> categoryDtos = new ArrayList<CategoryDto>();
        CategoryDto dto;
        Category category;
        for(int i=0; i<list.size(); i++){
            category = list.get(i);
            dto = new CategoryDto();
            if(null==category.getParentId()){
//                list.remove(i);
                selectSonCategory(category, dto, list, categoryDtos);
            }
        }
        return categoryDtos;
    }

    private void selectSonCategory(Category category,CategoryDto dto, List<Category> list,  List<CategoryDto> categoryDtos){
        dto.setImgUrl(category.getImgUrl());
        dto.setId(category.getId());
        dto.setIsLeaf(category.getIsLeaf());
        dto.setName(category.getName());
        dto.setParentId(category.getParentId());
        dto.setSeqNum(category.getSeqNum());
        dto.setStatus(category.getStatus());


        List<CategoryDto> dtos = new ArrayList<CategoryDto>();
        dto.setLists(dtos);
        categoryDtos.add(dto);
        CategoryDto dt;
        Category md;
        for(int i=0; i<list.size(); i++){
            md = list.get(i);
            if(category.getId().equals(md.getParentId())){
                dt = new CategoryDto();
//                list.remove(i);
                selectSonCategory(md, dt, list, dtos);
//                dt.setImgUrl(cat.getImgUrl());
//                dt.setId(cat.getId());
//                dt.setIsLeaf(cat.getIsLeaf());
//                dt.setName(cat.getName());
//                dt.setParentId(cat.getParentId());
//                dt.setSeqNum(cat.getSeqNum());
//                dt.setStatus(cat.getStatus());
//                dtos.add(dt);

            }
        }
    }

    @Override
    public List<CategoryDto> getLevelCategorys(Long id) throws Exception {
        List<Category> list = null;
        CategoryExample example = new CategoryExample();
//        example.createCriteria().andStatusEqualTo(StatusConstant.OK.getValue());
        CategoryExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(StatusConstant.OK.getValue());
        criteria.andTypeEqualTo("0");
        if(null==id){
            criteria.andParentIdIsNull();
        }else if(id==-1){
            criteria.andIsHotEqualTo(CategoryIsHotConstant.IS.getValue());
        }else {
            criteria.andParentIdEqualTo(id);
        }
        list = categoryMapper.selectByExample(example);
        if(null!=list && list.size()>0){
            List<CategoryDto> categoryDtos = new ArrayList<CategoryDto>();
//            CategoryDto dto;
            for (Category category : list){
//                dto = new CategoryDto();
//                dto.setImgUrl(category.getImgUrl());
//                dto.setId(category.getId());
//                dto.setIsLeaf(category.getIsLeaf());
//                dto.setName(category.getName());
//                dto.setParentId(category.getParentId());
//                dto.setSeqNum(category.getSeqNum());
//                dto.setStatus(category.getStatus());
                categoryDtos.add(getDto(category));
            }
            return categoryDtos;
        }
        return null;
    }

    @Override
    public List<CategoryDto> getItemTag() throws Exception {
        List<Category> list = null;
        CategoryExample example = new CategoryExample();
//        example.createCriteria().andStatusEqualTo(StatusConstant.OK.getValue());
        CategoryExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(StatusConstant.OK.getValue());
        criteria.andTypeEqualTo("1");
        list = categoryMapper.selectByExample(example);
        if(null!=list && list.size()>0){
            List<CategoryDto> categoryDtos = new ArrayList<CategoryDto>();
            for (Category category : list){
//                dto = new CategoryDto();
//                dto.setImgUrl(category.getImgUrl());
//                dto.setId(category.getId());
//                dto.setIsLeaf(category.getIsLeaf());
//                dto.setName(category.getName());
//                dto.setParentId(category.getParentId());
//                dto.setSeqNum(category.getSeqNum());
//                dto.setStatus(category.getStatus());
                categoryDtos.add(getDto(category));
            }
            return categoryDtos;
        }
        return null;
    }

    @Override
    public List<CategoryDto> getAllLeafCategory() throws Exception {
        List<Category> list = null;
        CategoryExample example = new CategoryExample();
//        example.createCriteria().andStatusEqualTo(StatusConstant.OK.getValue());
        CategoryExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo(StatusConstant.OK.getValue());
        criteria.andTypeEqualTo("0");
        criteria.andIsLeafEqualTo("1");
        list = categoryMapper.selectByExample(example);
        if(null!=list && list.size()>0){
            List<CategoryDto> categoryDtos = new ArrayList<CategoryDto>();
            for (Category category : list){
                categoryDtos.add(getDto(category));
            }
            return categoryDtos;
        }
        return null;
    }

    private CategoryDto getDto(Category category){
        CategoryDto dto = new CategoryDto();
        dto.setImgUrl(category.getImgUrl());
        dto.setId(category.getId());
        dto.setIsLeaf(category.getIsLeaf());
        dto.setName(category.getName());
        dto.setParentId(category.getParentId());
        dto.setSeqNum(category.getSeqNum());
        dto.setStatus(category.getStatus());
        return dto;
    }

    @Override
    public CategoryDto getCategory(Long id) throws Exception {
        Category category = categoryMapper.selectByPrimaryKey(id);
        if(null==category)
            return null;
        return getDto(category);
    }
}

