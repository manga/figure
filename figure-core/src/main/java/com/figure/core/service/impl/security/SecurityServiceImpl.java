package com.figure.core.service.impl.security;

import com.figure.common.utils.StringUtil;
import com.figure.core.dao.security.AccountMapper;
import com.figure.core.dao.security.ResourcesMapper;
import com.figure.core.dao.security.RoleMapper;
import com.figure.core.model.security.*;
import com.figure.service.dto.PageDto;
import com.figure.service.dto.security.AccountDto;
import com.figure.service.dto.security.ResourcesDto;
import com.figure.service.dto.security.RoleDto;
import com.figure.service.provide.security.ISecurityService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weber on 4/9/2016.
 */
@Service("securityService")
public class SecurityServiceImpl implements ISecurityService {

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    ResourcesMapper resourcesMapper;

    @Override
    public AccountDto findAccountByNames(String accountName) {
        AccountExample example = new AccountExample();
        example.createCriteria().andAccountNameEqualTo(accountName);
        List<Account> list = accountMapper.selectByExample(example);
        AccountDto dto = null;
        if(null!=list && list.size()>0){
            Account account = list.get(0);
            dto = new AccountDto();
            BeanUtils.copyProperties(account, dto);
        }
        return dto;
    }

    @Override
    public List<ResourcesDto> queryAliResources() {
        ResourcesExample example = new ResourcesExample();
        List<Resources> list = resourcesMapper.selectByExample(example);
        List<ResourcesDto> lists = null;
        if(null!=list && list.size()>0){
            ResourcesDto dto = null;
            lists = new ArrayList<ResourcesDto>();
            for(Resources resources : list){
                dto = new ResourcesDto();
                BeanUtils.copyProperties(resources, dto);
                lists.add(dto);
            }
        }
        return lists;
    }

    @Override
    public List<ResourcesDto> findUserResourcess(String accountId) {
        List<Resources> list = resourcesMapper.findUserResourcess(new Long(accountId));
        List<ResourcesDto> lists = null;
        if(null!=list && list.size()>0){
            ResourcesDto dto = null;
            lists = new ArrayList<ResourcesDto>();
            for(Resources resources : list){
                dto = new ResourcesDto();
                BeanUtils.copyProperties(resources, dto);
                lists.add(dto);
            }
        }
        return lists;
    }

    @Override
    public void addResources(ResourcesDto dto) {

    }

    @Override
    public void updateResources(ResourcesDto dto) {

    }

    @Override
    public PageDto<RoleDto> queryRole(String name, Integer status, int pageNo, int pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        RoleExample example = new RoleExample();
        if(StringUtil.isNotEmpty(name)){
            example.createCriteria().andNameLike(name);
        }
        if(StringUtil.isNotEmpty(name)){
            example.createCriteria().andStatusEqualTo(status);
        }
        Page<Role> list = (Page<Role>) roleMapper.selectByExample(example);
        List<RoleDto> lists = new ArrayList<RoleDto>();
        if (null != list && list.size() > 0) {
            RoleDto dto;
            for (Role role : list) {
                dto = new RoleDto();
                BeanUtils.copyProperties(role, dto);
                lists.add(dto);
            }
        }
        PageDto<RoleDto> pageDto = new PageDto<RoleDto>(list.getPageNum(), list.getPageSize(),
                list.getTotal(), list.getPages(), lists);
        return pageDto;
    }

    @Override
    public void addRole(RoleDto dto) {
        if(null==dto)
            return;
        Role role = new Role();
        BeanUtils.copyProperties(dto, role);
        roleMapper.insert(role);
    }

    @Override
    public void updateRole(RoleDto dto) {
        if(null==dto || null==dto.getId())
            return;
        Role role = new Role();
        BeanUtils.copyProperties(dto, role);
        roleMapper.updateByPrimaryKeySelective(role);
    }
}
