package com.figure.core.service.impl.user;

import com.figure.common.utils.StringUtil;
import com.figure.core.dao.user.UserLoginMapper;
import com.figure.core.dao.user.UserMapper;
import com.figure.core.model.user.User;
import com.figure.core.model.user.UserLogin;
import com.figure.service.dto.user.UserDto;
import com.figure.service.provide.user.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.Date;

/**
 * Created by chuanbo.wei on 2016/9/1.
 */
@Service("userService")
public class UserServiceImpl implements IUserService{

    @Autowired
    UserLoginMapper userLoginMapper;

    @Autowired
    UserMapper userMapper;

    @Override
    public void login(UserDto dto) throws Exception {
        if(null==dto || StringUtil.isEmpty(dto.getId())){
            return;
        }
        UserLogin record = new UserLogin();
        record.setUserId(dto.getId());
//        record.setId();
        record.setLoginIp(dto.getIp());
        record.setLoginTime(new Date());
        userLoginMapper.insert(record);

        User user = userMapper.selectByPrimaryKey(dto.getId());
        if(null==user){
            user = new User();
            user.setId(dto.getId());
            user.setAddress(dto.getAddress());
            user.setAge(dto.getAge());
            user.setCreateTime(new Date());
            user.setEmail(dto.getEmail());
            user.setImgUrl(dto.getImgUrl());
            user.setNick(dto.getNick());
            user.setMobilePhone(dto.getMobilePhone());
            user.setUserName(dto.getUserName());
            user.setPwd(dto.getPwd());
            userMapper.insert(user);
        }
    }
}
