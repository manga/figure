package com.figure.service.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PageDto<E> implements Serializable {

    private int pageNum;
    private int pageSize;
    private long total;
    private int pages;
    private List<E> resultList = new ArrayList<E>();

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<E> getResultList() {
        return resultList;
    }

    public void setResultList(List<E> resultList) {
        this.resultList = resultList;
    }

    public PageDto() {

    }

    public PageDto(int pageNum, int pageSize, long total, int pages, List<E> resultList) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.total = total;
        this.pages = pages;
        this.resultList = resultList;
    }
}
