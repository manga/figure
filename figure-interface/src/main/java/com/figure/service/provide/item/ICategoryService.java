package com.figure.service.provide.item;

import com.figure.service.dto.item.CategoryDto;

import java.util.List;

/**
 * Created by chuanbo.wei on 2016/9/1.
 */
public interface ICategoryService {

//    public void insert();
//
//    public void delete();

    /**
     * 获取所有分类信息，按层级结构返回
     * @return
     * @throws Exception
     */
    public List<CategoryDto> getCategoryList() throws Exception;

    /**
     * 返回指定分类的所有子分类，id为空时，返回第一级分类
     * @return
     * @throws Exception
     */
    public List<CategoryDto> getLevelCategorys(Long id) throws Exception;

    /**
     * 获取标签记录
     * @return
     * @throws Exception
     */
    public List<CategoryDto> getItemTag() throws Exception;

    /**
     * 获取所有叶子类目
     * @return
     * @throws Exception
     */
    public List<CategoryDto> getAllLeafCategory() throws Exception;

    /**
     * 根据id获取分类信息
     * @param id
     * @return
     * @throws Exception
     */
    public CategoryDto getCategory(Long id) throws Exception;
}
