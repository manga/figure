package com.figure.service.provide.item;

import com.figure.service.dto.PageDto;
import com.figure.service.dto.item.*;

import java.util.List;

/**
 * Created by chuanbo.wei on 2016/8/31.
 */
public interface IItemService {

    /**
     * taobao.atb.items.get (爱淘宝商品查询)
     * @param area  商品所在地
     * @param autoSend  是否自动发货
     * @param cashCoupon 是否支持抵价券，设置为true表示该商品支持抵价券，设置为false或不设置表示不判断这个属性
     * @param cid   标准商品后台类目id。该ID可以通过taobao.itemcats.get接口获取到。
     * @param endCommissionNum  30天累计推广量（与返回数据中的commission_num字段对应）上限.
     * @param endCommissionRate 佣金比率上限，如：2345表示23.45%。注：start_commissionRate和end_commissionRate一起设置才有效。
     * @param endCredit 可选值和start_credit一样.start_credit的值一定要小于或等于end_credit的值。注：end_credit与start_credit一起使用才生效
     * @param endPrice  最高价格
     * @param endTotalnum   商品总成交量（与返回字段volume对应）上限。
     * @param fields    需返回的字段列表.可open_iid,title,nick,pic_url,price,commission,commission_rate,commission_num,commission_volume,seller_credit_score,item_location,volume,seller_id,title,coupon_rate,coupon_price,coupon_start_time,coupon_end_time,shop_type,promotion_price
     * @param guarantee 是否查询消保卖家
     * @param keyword   商品标题中包含的关键字. 注意:查询时keyword,cid至少选择其中一个参数
     * @param mallItem  是否商城的商品，设置为true表示该商品是属于淘宝商城的商品，设置为false或不设置表示不判断这个属性
     * @param onemonthRepair    是否30天维修，设置为true表示该商品是支持30天维修，设置为false或不设置表示不判断这个属性
     * @param overseasItem  是否海外商品，设置为true表示该商品是属于海外商品，默认为false
     * @param pageNo    结果页数.1~10 支持最大值为：10
     * @param pageSize  每页返回结果数.最大每页40 支持最大值为：400
     * @param realDescribe  是否如实描述(即:先行赔付)商品，设置为true表示该商品是如实描述的商品，设置为false或不设置表示不判断这个属性
     * @param sevendaysReturn   是否支持7天退换，设置为true表示该商品支持7天退换，设置为false或不设置表示不判断这个属性
     * @param sort  默认排序:default price_desc(价格从高到低) price_asc(价格从低到高) credit_desc(信用等级从高到低)
     *              commissionRate_desc(佣金比率从高到低) commissionRate_asc(佣金比率从低到高) commissionNum_desc(成交量成高到低)
     *              commissionNum_asc(成交量从低到高) commissionVolume_desc(总支出佣金从高到低)
     *              commissionVolume_asc(总支出佣金从低到高) delistTime_desc(商品下架时间从高到低) delistTime_asc(商品下架时间从低到高)
     * @param startCommissionNum    30天累计推广量（与返回数据中的commission_num字段对应）下限.注：该字段要与end_commissionNum一起使用才生效
     * @param startCommissionRate   佣金比率下限，如：1234表示12.34%
     * @param startCredit   卖家信用: 1heart(一心) 2heart (两心) 3heart(三心) 4heart(四心) 5heart(五心) 1diamond(一钻) 2diamond(两钻 3diamond(三钻)
     *                      4diamond(四钻) 5diamond(五钻) 1crown(一冠) 2crown(两冠) 3crown(三冠) 4crown(四冠) 5crown(五冠)
     *                      1goldencrown(一黄冠) 2goldencrown(二黄冠) 3goldencrown(三黄冠) 4goldencrown(四黄冠) 5goldencrown(五黄冠)
     * @param startPrice    起始价格.传入价格参数时,需注意起始价格和最高价格必须一起传入,并且 start_price <= end_price
     * @param startTotalnum 商品总成交量（与返回字段volume对应）下限。
     * @param supportCod    是否支持货到付款，设置为true表示该商品是支持货到付款，设置为false或不设置表示不判断这个属性
     * @param vipCard   是否支持VIP卡，设置为true表示该商品支持VIP卡，设置为false或不设置表示不判断这个属性
     * @return
     */
    public int addAtbItems(String area, String autoSend, String cashCoupon, Long cid, String endCommissionNum,String endCommissionRate,String endCredit,
                            String endPrice,String endTotalnum,String fields,String guarantee,String keyword,String mallItem,
                            String onemonthRepair,String overseasItem,Long pageNo, Long pageSize, String realDescribe,String sevendaysReturn,
                            String sort,String startCommissionNum,String startCommissionRate,String startCredit,String startPrice,
                            String startTotalnum,String supportCod, String vipCard);

    /**
     * 爱淘宝折扣商品查询 (taobao.atb.items.coupon.get)
     * @param area  商品所在地
     * @param cid   标准商品后台类目id。该ID可以通过taobao.itemcats.get接口获取到。
     * @param couponType 优惠商品类型.1:打折商品,默认值为1
     * @param endCommissionNum  30天累计推广量（与返回数据中的commission_num字段对应）上限.
     * @param endCommissionRate 佣金比率上限，如：2345表示23.45%。注：start_commissionRate和end_commissionRate一起设置才有效。
     * @param endCommissionVolume   最高累计推广佣金选项
     * @param endCouponRate 设置折扣比例范围上限,如：8000表示80.00%.注：要起始折扣比率和最高折扣比率一起设置才有效
     * @param endCredit 可选值和start_credit一样.start_credit的值一定要小于或等于end_credit的值。注：end_credit与start_credit一起使用才生效
     * @param endVolume 设置商品总成交量（与返回字段volume对应）上限
     * @param fields    需返回的字段列表.可选值:open_iid,title,nick,pic_url,price,commission,commission_rate,commission_num,commission_volume,seller_credit_score,item_location,volume,coupon_price,coupon_rate,coupon_start_time,coupon_end_time,shop_type;字段之间用","分隔
     * @param keyword   商品标题中包含的关键字. 注意:查询时keyword,cid至少选择其中一个参数
    //     * @param mallItem  是否商城的商品，设置为true表示该商品是属于淘宝商城的商品，设置为false或不设置表示不判断这个属性
    //     * @param onemonthRepair    是否30天维修，设置为true表示该商品是支持30天维修，设置为false或不设置表示不判断这个属性
    //     * @param overseasItem  是否海外商品，设置为true表示该商品是属于海外商品，默认为false
     * @param pageNo    结果页数.1~10 支持最大值为：10
     * @param pageSize  每页返回结果数.最大每页40 支持最大值为：400
     * @param shopType  店铺类型.默认all,商城:b,集市:c
    //     * @param sevendaysReturn   是否支持7天退换，设置为true表示该商品支持7天退换，设置为false或不设置表示不判断这个属性
     * @param sort  默认排序:default price_desc(价格从高到低) price_asc(价格从低到高) credit_desc(信用等级从高到低)
     *              commissionRate_desc(佣金比率从高到低) commissionRate_asc(佣金比率从低到高) commissionNum_desc(成交量成高到低)
     *              commissionNum_asc(成交量从低到高) commissionVolume_desc(总支出佣金从高到低)
     *              commissionVolume_asc(总支出佣金从低到高) delistTime_desc(商品下架时间从高到低) delistTime_asc(商品下架时间从低到高)
     * @param startCommissionNum    30天累计推广量（与返回数据中的commission_num字段对应）下限.注：该字段要与end_commissionNum一起使用才生效
     * @param startCommissionRate   佣金比率下限，如：1234表示12.34%
     * @param startCommissionVolume   起始累计推广量佣金.注：返回的数据是30天内累计推广佣金，该字段要与最高累计推广佣金一起使用才生效
     * @param startCouponRate   设置折扣比例范围下限,如：7000表示70.00%
     * @param startCredit   卖家信用: 1heart(一心) 2heart (两心) 3heart(三心) 4heart(四心) 5heart(五心) 1diamond(一钻) 2diamond(两钻 3diamond(三钻)
     *                      4diamond(四钻) 5diamond(五钻) 1crown(一冠) 2crown(两冠) 3crown(三冠) 4crown(四冠) 5crown(五冠)
     *                      1goldencrown(一黄冠) 2goldencrown(二黄冠) 3goldencrown(三黄冠) 4goldencrown(四黄冠) 5goldencrown(五黄冠)
    //     * @param startPrice    起始价格.传入价格参数时,需注意起始价格和最高价格必须一起传入,并且 start_price <= end_price
     * @param startVolume 设置商品总成交量（与返回字段volume对应）下限。
    //     * @param supportCod    是否支持货到付款，设置为true表示该商品是支持货到付款，设置为false或不设置表示不判断这个属性
    //     * @param vipCard   是否支持VIP卡，设置为true表示该商品支持VIP卡，设置为false或不设置表示不判断这个属性
     * @return
     */
    public int addAtbItemsCoupon(String area,Long cid, Long couponType, Long endCommissionNum,Long endCommissionRate,Long endCommissionVolume,Long endCouponRate,
                                 String endCredit,Long endVolume,String fields,String keyword,Long pageNo, Long pageSize, String shopType,
                                 String sort,Long startCommissionNum,Long startCommissionRate,Long startCommissionVolume,Long startCouponRate,
                                 String startCredit,Long startVolume);

    /**
     * taobao.atb.items.get (爱淘宝商品查询)
     * @param area  商品所在地
     * @param autoSend  是否自动发货
     * @param cashCoupon 是否支持抵价券，设置为true表示该商品支持抵价券，设置为false或不设置表示不判断这个属性
     * @param cid   标准商品后台类目id。该ID可以通过taobao.itemcats.get接口获取到。
     * @param endCommissionNum  30天累计推广量（与返回数据中的commission_num字段对应）上限.
     * @param endCommissionRate 佣金比率上限，如：2345表示23.45%。注：start_commissionRate和end_commissionRate一起设置才有效。
     * @param endCredit 可选值和start_credit一样.start_credit的值一定要小于或等于end_credit的值。注：end_credit与start_credit一起使用才生效
     * @param endPrice  最高价格
     * @param endTotalnum   商品总成交量（与返回字段volume对应）上限。
     * @param fields    需返回的字段列表.可open_iid,title,nick,pic_url,price,commission,commission_rate,commission_num,commission_volume,seller_credit_score,item_location,volume,seller_id,title,coupon_rate,coupon_price,coupon_start_time,coupon_end_time,shop_type,promotion_price
     * @param guarantee 是否查询消保卖家
     * @param keyword   商品标题中包含的关键字. 注意:查询时keyword,cid至少选择其中一个参数
     * @param mallItem  是否商城的商品，设置为true表示该商品是属于淘宝商城的商品，设置为false或不设置表示不判断这个属性
     * @param onemonthRepair    是否30天维修，设置为true表示该商品是支持30天维修，设置为false或不设置表示不判断这个属性
     * @param overseasItem  是否海外商品，设置为true表示该商品是属于海外商品，默认为false
     * @param pageNo    结果页数.1~10 支持最大值为：10
     * @param pageSize  每页返回结果数.最大每页40 支持最大值为：400
     * @param realDescribe  是否如实描述(即:先行赔付)商品，设置为true表示该商品是如实描述的商品，设置为false或不设置表示不判断这个属性
     * @param sevendaysReturn   是否支持7天退换，设置为true表示该商品支持7天退换，设置为false或不设置表示不判断这个属性
     * @param sort  默认排序:default price_desc(价格从高到低) price_asc(价格从低到高) credit_desc(信用等级从高到低)
     *              commissionRate_desc(佣金比率从高到低) commissionRate_asc(佣金比率从低到高) commissionNum_desc(成交量成高到低)
     *              commissionNum_asc(成交量从低到高) commissionVolume_desc(总支出佣金从高到低)
     *              commissionVolume_asc(总支出佣金从低到高) delistTime_desc(商品下架时间从高到低) delistTime_asc(商品下架时间从低到高)
     * @param startCommissionNum    30天累计推广量（与返回数据中的commission_num字段对应）下限.注：该字段要与end_commissionNum一起使用才生效
     * @param startCommissionRate   佣金比率下限，如：1234表示12.34%
     * @param startCredit   卖家信用: 1heart(一心) 2heart (两心) 3heart(三心) 4heart(四心) 5heart(五心) 1diamond(一钻) 2diamond(两钻 3diamond(三钻)
     *                      4diamond(四钻) 5diamond(五钻) 1crown(一冠) 2crown(两冠) 3crown(三冠) 4crown(四冠) 5crown(五冠)
     *                      1goldencrown(一黄冠) 2goldencrown(二黄冠) 3goldencrown(三黄冠) 4goldencrown(四黄冠) 5goldencrown(五黄冠)
     * @param startPrice    起始价格.传入价格参数时,需注意起始价格和最高价格必须一起传入,并且 start_price <= end_price
     * @param startTotalnum 商品总成交量（与返回字段volume对应）下限。
     * @param supportCod    是否支持货到付款，设置为true表示该商品是支持货到付款，设置为false或不设置表示不判断这个属性
     * @param vipCard   是否支持VIP卡，设置为true表示该商品支持VIP卡，设置为false或不设置表示不判断这个属性
     * @return
     */
    public PageDto<AtbItemDto> getAtbItems(String area, String autoSend, String cashCoupon, Long cid, String endCommissionNum,String endCommissionRate,String endCredit,
                           String endPrice,String endTotalnum,String fields,String guarantee,String keyword,String mallItem,
                           String onemonthRepair,String overseasItem,Long pageNo, Long pageSize, String realDescribe,String sevendaysReturn,
                           String sort,String startCommissionNum,String startCommissionRate,String startCredit,String startPrice,
                           String startTotalnum,String supportCod, String vipCard);

    /**
     * 获取banner信息
     * @return
     * @throws Exception
     */
    public List<BannerDto> getBannerDto()throws Exception;

    /**
     * 获取热销商品
     * @return
     * @throws Exception
     */
    public PageDto<HotItemDto> getHotItems(int pageNo, int pageSize)throws Exception;

    /**
     * 获取推荐商品
     * @return
     * @throws Exception
     */
    public PageDto<RecommendItemDto> getRecommendItems(int pageNo, int pageSize)throws Exception;

    /**
     * 商品查询
     * @param pageNo
     * @param pageSize
     * @param title 商品名称，模糊查询用
     * @param orderType 0:30天内交易量降序  1:卖家信用等级降序  2:促销价格升序 3:促销价格降序
     * @param categoryId 类目id
     * @return
     * @throws Exception
     */
    public PageDto<FigureItemDto> getFigureItems(int pageNo, int pageSize, String title, String orderType, Long categoryId) throws Exception;

    /**
     * 为选取的商品打上类目或标签
     * @param list
     * @param categoryIds
     * @throws Exception
     */
    public void saveItemCategory(List<FigureItemDto> list, Long[] categoryIds) throws Exception;

    /**
     * 保存热销商品
     * @param list
     * @throws Exception
     */
    public void saveHotItems(List<FigureItemDto> list) throws Exception;

    /**
     * 保存推荐商品
     * @param list
     * @throws Exception
     */
    public void saveRecommendItems(List<FigureItemDto> list) throws Exception;

    /**
     * 保存banner信息
     * @param dto
     * @throws Exception
     */
    public void saveBanner(BannerDto dto) throws Exception;

    /**
     * 根据openIid获取商品信息
     * @param openIid
     * @return
     * @throws Exception
     */
    public FigureItemDto getFigureItem(String openIid) throws Exception;
}
