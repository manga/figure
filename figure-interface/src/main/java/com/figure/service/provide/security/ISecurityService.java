package com.figure.service.provide.security;



import com.figure.service.dto.PageDto;
import com.figure.service.dto.security.AccountDto;
import com.figure.service.dto.security.ResourcesDto;
import com.figure.service.dto.security.RoleDto;

import java.util.List;

/**
 * Created by Weber on 4/9/2016.
 */
public interface ISecurityService {

    /**
     *
     * @param userName
     * @return
     */
    public AccountDto findAccountByNames(String userName);

    public List<ResourcesDto> queryAliResources();

    /**
     * 获取用户拥有的权限
     * @param accountId
     * @return
     */
    public List<ResourcesDto> findUserResourcess(String accountId);

    /**
     * 新增Resources
     * @param dto
     * @return
     */
    public void addResources(ResourcesDto dto);

    /**
     * 更新Resources
     * @param dto
     * @return
     */
    public void updateResources(ResourcesDto dto);

    /**
     * 查询role
     * @param name
     * @param status
     * @return
     */
    public PageDto<RoleDto> queryRole(String name, Integer status, int pageNo, int pageSize);

    /**
     * 新增role
     * @param dto
     * @return
     */
    public void addRole(RoleDto dto);

    /**
     * 更新role
     * @param dto
     * @return
     */
    public void updateRole(RoleDto dto);
}
