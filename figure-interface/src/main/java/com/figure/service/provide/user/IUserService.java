package com.figure.service.provide.user;

import com.figure.service.dto.user.UserDto;

/**
 * Created by chuanbo.wei on 2016/9/1.
 */
public interface IUserService {

    /**
     * 用户登录回调，插入用户登录记录，第一次登录的用户，在用户表中生成用户记录
     * @param dto
     * @throws Exception
     */
    public void login(UserDto dto) throws Exception;

}
