package com.figure.web.controller;


import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Weber on 4/5/2016.
 */
public class BaseController {

    public static final String USER_SESSION_KEY="user";
    protected static String JSPDIR = "/WEB-INF/html";
    protected static String CODE = "code";
    protected static String SUCCESS = "success";
    protected static String DATA = "rows";
    protected static String MSG = "msg";
    protected static String STATUS = "status";
    /**
     * 获取登录账号的ID
     *
     * @author lanyuan Email：mmm333zzz520@163.com date：2014-11-27
     * @param request
     * @return
     */
    public static String findUserSessionId(HttpServletRequest request) {
        Object obj = request.getSession().getAttribute("userSessionId");
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    /**
     * 获取登录账号的的对象
     *
     * @author lanyuan Email：mmm333zzz520@163.com date：2014-2-27
     * @param request
     * @return Object 返回是Object..需要转型为(Account)Object
     */
    public static Object findUserSession(HttpServletRequest request) {
        return (Object) request.getSession().getAttribute("userSession");
    }

    public static Object ok(Object data,ModelMap dataMap) {
//        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(STATUS, new Status(0, SUCCESS));
//        dataMap.put(SUCCESS, true);
        dataMap.put(DATA, data);
        return dataMap;
    }

    public static Object faile(int code, String msg, ModelMap dataMap) {
//        Map<String, Object> dataMap = new HashMap<String, Object>();
//        dataMap.put(CODE, code);
//        dataMap.put(SUCCESS, false);
//        dataMap.put(MSG, msg);
        dataMap.put(STATUS, new Status(code, msg));
        dataMap.put(DATA, null);
        return dataMap;
    }

    /**
     * 得到服务全路径，例如：http://127.0.0.1:8080/mylearningii
     *
     * @return 服务全路径
     */
    public String getBasePath(HttpServletRequest request) {
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName();

        if ( request.getServerPort() != 80 ) {
            basePath += ":" + request.getServerPort() + path;
        } else {
            basePath += path;
        }
        return basePath;
    }

    public static class Status {
        public static final int CODE_OK = 0;

        private int code;
        private String msg;

        public Status() {

        }

        public Status(int code, String msg) {
            super();
            this.code = code;
            this.msg = msg;
        }

        /**
         * @return the code
         */
        public int getCode() {
            return code;
        }

        /**
         * @param code
         *            the code to set
         */
        public void setCode(int code) {
            this.code = code;
        }

        /**
         * @return the message
         */
        public String getMsg() {
            return msg;
        }

        /**
         * @param msg
         *            the message to set
         */
        public void setMsg(String msg) {
            this.msg = msg;
        }

    }
}
