package com.figure.web.controller.item;

import com.figure.service.dto.item.CategoryDto;
import com.figure.service.provide.item.ICategoryService;
import com.figure.web.controller.BaseController;
import com.figure.web.util.APIResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by chuanbo.wei on 2016/9/1.
 */
@Controller
@RequestMapping("/category")
public class CategoryController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    private ICategoryService categoryService;

    @RequestMapping(value = {"/getAllCategory"})
    public APIResult getAllCategory() {
        List<CategoryDto> categoryDtos;
        try {
            categoryDtos = categoryService.getCategoryList();
        } catch (Exception e) {
            return APIResult.failResult(101, "获取所有分类异常！");
        }
        return APIResult.okResult(categoryDtos);
    }

    @RequestMapping(value = {"/getLevelCategorys"})
    public APIResult getLevelCategorys(@RequestParam(value = "id", required = false)Long id) {
        List<CategoryDto> categoryDtos;
        try {
            categoryDtos = categoryService.getLevelCategorys(id);
        } catch (Exception e) {
            return APIResult.failResult(101, "获取所有分类异常！");
        }
        return APIResult.okResult(categoryDtos);
    }

    @RequestMapping(value = {"/getItemTag"})
    public APIResult getItemTag() {
        List<CategoryDto> categoryDtos;
        try {
            categoryDtos = categoryService.getItemTag();
        } catch (Exception e) {
            return APIResult.failResult(101, "获取所有标签异常！");
        }
        return APIResult.okResult(categoryDtos);
    }

    @RequestMapping(value = {"/getAllLeafCategory"})
    public APIResult getAllLeafCategory() {
        List<CategoryDto> categoryDtos;
        try {
            categoryDtos = categoryService.getAllLeafCategory();
        } catch (Exception e) {
            return APIResult.failResult(101, "获取所有叶子类目异常！");
        }
        return APIResult.okResult(categoryDtos);
    }
}
