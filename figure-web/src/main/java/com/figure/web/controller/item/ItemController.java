package com.figure.web.controller.item;

import com.alibaba.fastjson.JSON;
import com.figure.common.utils.StringUtil;
import com.figure.core.model.item.FigureItem;
import com.figure.service.dto.PageDto;
import com.figure.service.dto.item.*;
import com.figure.service.provide.item.IItemService;
import com.figure.web.controller.BaseController;
import com.figure.web.util.APIResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


/**
 * Created by Weber on 1/19/2016.
 */
@Controller
@RequestMapping("/item")
public class ItemController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    private IItemService itemService;

    //open_iid,title,nick,pic_url,price,commission,commission_rate,commission_num,commission_volume,seller_credit_score,item_location,volume,seller_id,title,coupon_rate,coupon_price,coupon_start_time,coupon_end_time,shop_type,promotion_price
//    @RequestMapping(value = {"/addAtbItems"})
    public APIResult addAtbItems(String area, String autoSend, String cashCoupon, Long cid, String endCommissionNum,String endCommissionRate,String endCredit,
                                 String endPrice,String endTotalnum,@RequestParam(value = "fields", required = true)String fields,String guarantee,String keyword,String mallItem,
                                 String onemonthRepair,String overseasItem,String pageNo, String pageSize, String realDescribe,String sevendaysReturn,
                                 String sort,String startCommissionNum,String startCommissionRate,String startCredit,String startPrice,
                                 String startTotalnum,String supportCod, String vipCard) {
        Long pn = null;
        Long ps = null;
        try {
            if(StringUtil.isEmpty(pageNo)){
                pn = new Long(1);
            }else {
                pn = new Long(pageNo);
            }
            if(StringUtil.isEmpty(pageSize)){
                ps = new Long(50);
            }else {
                ps = new Long(pageSize);
            }

            itemService.addAtbItems(area, autoSend,cashCoupon,cid,endCommissionNum,endCommissionRate,endCredit,endPrice,endTotalnum
                    ,fields,
                    guarantee,keyword,mallItem,onemonthRepair,overseasItem,pn, ps,realDescribe,sevendaysReturn,
                    sort,startCommissionNum,startCommissionRate,startCredit,startPrice,startTotalnum,supportCod,vipCard);
        } catch (Exception e) {
            logger.error("插入商品异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(100, "插入商品异常！");
        }
        return APIResult.okResult(null);
    }

    @RequestMapping(value = {"/getAtbItems"})
    public APIResult getAtbItems(ModelMap modelMap,String area, String autoSend, String cashCoupon, Long cid, String endCommissionNum,
                                 String endCommissionRate, String endCredit, String endPrice, String endTotalnum, String fields,
                                 String guarantee, String keyword, String mallItem, String onemonthRepair, String overseasItem,
                                 String pageNo, String pageSize, String realDescribe, String sevendaysReturn, String sort, String startCommissionNum,
                                 String startCommissionRate, String startCredit, String startPrice, String startTotalnum, String supportCod, String vipCard) {
        Long pn = null;
        Long ps = null;
        PageDto<AtbItemDto> list = null;
        try {
            if(StringUtil.isEmpty(pageNo)){
                pn = new Long(1);
            }else {
                pn = new Long(pageNo);
            }
            if(StringUtil.isEmpty(pageSize)){
                ps = new Long(20);
            }else {
                ps = new Long(pageSize);
            }
            fields="open_iid,title,nick,pic_url,price,commission,commission_rate,commission_num,commission_volume,seller_credit_score,item_location,volume,seller_id,title,coupon_rate,coupon_price,coupon_start_time,coupon_end_time,shop_type,promotion_price";
            list = itemService.getAtbItems(area, autoSend, cashCoupon, cid, endCommissionNum,
                    endCommissionRate, endCredit, endPrice, endTotalnum, fields,
                    guarantee, keyword, mallItem, onemonthRepair, overseasItem,
                    pn, ps, realDescribe, sevendaysReturn, sort, startCommissionNum,
                    startCommissionRate, startCredit, startPrice, startTotalnum, supportCod, vipCard);
//            if(null!=list){
//                modelMap.put("total", list.getTotal());
//            }else {
//                modelMap.put("total", "0");
//            }
        } catch (Exception e) {
            logger.error("查询商品异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(101, "查询商品异常！");
//            faile(101, "查询商品异常！", modelMap);
//            return "fail";
        }
        return APIResult.okResult(list);
//        ok(list.getResultList(), modelMap);
//        return "success";
    }

//    @RequestMapping(value = {"/addAtbItemsCoupon"})
    public APIResult addAtbItemsCoupon(String pageNo, String pageSize, String keyword) {
        Long pn = null;
        Long ps = null;
        try {
            if(StringUtil.isEmpty(pageNo)){
                pn = new Long(1);
            }else {
                pn = new Long(pageNo);
            }
            if(StringUtil.isEmpty(pageSize)){
                ps = new Long(50);
            }else {
                ps = new Long(pageSize);
            }

            itemService.addAtbItemsCoupon(null, null, null, null, null, null, null, null, null,
                    "open_iid,title,nick,pic_url,price,commission,commission_rate,commission_num,commission_volume,seller_credit_score,item_location,volume,coupon_price,coupon_rate,coupon_start_time,coupon_end_time,shop_type",
                    keyword, pn, ps, null,
                    null, null, null, null, null, null, null);
        } catch (Exception e) {
            logger.error("插入爱淘宝折扣商品异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(102, "插入爱淘宝折扣商品异常！");
        }
        return APIResult.okResult(null);
    }

    @RequestMapping(value = {"/getBanners"})
    public APIResult getBanners() {
        List<BannerDto> bannerDtos=null;
        try {
            bannerDtos = itemService.getBannerDto();
        } catch (Exception e) {
            logger.error("查询Banner异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(103, "查询Banner异常！");
        }
        return APIResult.okResult(bannerDtos);
    }

    @RequestMapping(value = {"/getHotItems"})
    public APIResult getHotItems(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                                 @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        if (pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize < 1 && pageSize > 100) {
            pageSize = 20;
        }
        PageDto<HotItemDto> hotItemDtos=null;
        try {
            hotItemDtos = itemService.getHotItems(pageNo, pageSize);
        } catch (Exception e) {
            logger.error("查询推荐品异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(104, "查询推荐品异常！");
        }
        return APIResult.okResult(hotItemDtos);
    }

    @RequestMapping(value = {"/getRecommendItems"})
    public APIResult getRecommendItems(@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                                       @RequestParam(value = "pageSize", defaultValue = "20") int pageSize) {
        if (pageNo < 1) {
            pageNo = 1;
        }
        if (pageSize < 1 && pageSize > 100) {
            pageSize = 20;
        }
        PageDto<RecommendItemDto> recommendItemDtos=null;
        try {
            recommendItemDtos = itemService.getRecommendItems(pageNo, pageSize);
        } catch (Exception e) {
            logger.error("查询热销品异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(105, "查询热销品异常！");
        }
        return APIResult.okResult(recommendItemDtos);
    }

    @RequestMapping(value = {"/getFigureItems"})
    public APIResult getFigureItems(ModelMap modelMap,@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
                                 @RequestParam(value = "pageSize", defaultValue = "20") int pageSize, String title, String orderType, String categoryId) {
//        Long pn = null;
//        Long ps = null;
//        if(StringUtil.isEmpty(categoryId)){
//            return APIResult.failResult(109, "参数categoryId不能为空");
//        }
        PageDto<FigureItemDto> list = null;
        Long cid = null;
        try {
//            if(StringUtil.isEmpty(pageNo)){
//                pn = new Long(1);
//            }else {
//                pn = new Long(pageNo);
//            }
//            if(StringUtil.isEmpty(pageSize)){
//                ps = new Long(50);
//            }else {
//                ps = new Long(pageSize);
//            }
            if(StringUtil.isNotEmpty(categoryId)){
                cid = new Long(categoryId);
            }
            list = itemService.getFigureItems(pageNo, pageSize, title, orderType, cid);
//            if(null!=list){
//                modelMap.put("total", list.getTotal());
//            }else {
//                modelMap.put("total", "0");
//                ok(null, modelMap);
//                return "success";
//            }
        } catch (Exception e) {
            logger.error("查询商品异常！" + e.getMessage());
            e.printStackTrace();
            return APIResult.failResult(101, "查询商品异常！");
//            faile(105, "查询商品异常！", modelMap);
//            return "fail";
        }
//        return APIResult.okResult(list);
//        ok(list.getResultList(), modelMap);
//        return "success";
        return APIResult.okResult(list);
    }
}
