package com.figure.web.controller.user;

import com.figure.service.dto.item.CategoryDto;
import com.figure.service.dto.user.UserDto;
import com.figure.service.provide.user.IUserService;
import com.figure.web.controller.BaseController;
import com.figure.web.util.APIResult;
import com.figure.web.util.NetworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by chuanbo.wei on 2016/9/1.
 */
@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping(value = {"/login"})
    public APIResult login(HttpServletRequest request, @RequestParam(value = "id", required = true) String id,
                           @RequestParam(value = "nick", required = true)String nick, @RequestParam(value = "imgUrl", required = true)String imgUrl,
                           @RequestParam(value = "pwd", required = false)String pwd,
                           @RequestParam(value = "mobilePhone", required = false)String mobilePhone,
                           @RequestParam(value = "age", required = false)Integer age,
                           @RequestParam(value = "sex", required = false)String sex,
                           @RequestParam(value = "usrName", required = false)String usrName,
                           @RequestParam(value = "email", required = false)String email) {
        UserDto dto = null;
        try {
            dto = new UserDto();
            dto.setId(id);
//            dto.setAddress(a);
            dto.setEmail(email);
            dto.setPwd(pwd);
            dto.setImgUrl(imgUrl);
            dto.setIp(NetworkUtil.getIpAddr(request));
            dto.setMobilePhone(mobilePhone);
            dto.setNick(nick);
            dto.setAge(age);
            dto.setSex(sex);
            dto.setUserName(usrName);
            userService.login(dto);
        } catch (Exception e) {
            return APIResult.failResult(101, "用户回调失败！");
        }
        return APIResult.okResult(null);
    }
}
