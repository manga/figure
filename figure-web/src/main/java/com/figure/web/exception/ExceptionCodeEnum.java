/**
 * autonavi.com Inc.
 * Copyright (c) 2004-2014 All Rights Reserved.
 */
package com.figure.web.exception;

/**
 * 定义系统所有异常
 */
public enum ExceptionCodeEnum {
    UNKNOW_EXCEPTION(1001, "服务器忙,请稍后再试!"),
    TAOBAOID_VIN_ISNULL(1002, "淘宝id和vin不能都为空!"),
    HISPOIID_MUST_INPUT(1003, "历史poi的id,为必输条件!"),
    PARAM_MUST_INPUT(1004, "参数%s不能为空!"),  //使用String.format(,)格式化.
    PARAM_MUST_NUMBER(1005, "参数%s必须为正数!"),   //使用String.format(,)格式化.
    ISFREE_MUST_IN_0_1_2(1006, "参数isFree必须为0/1/2中的一个!"),
    ID_ISNOTEXIST_INPUT(1007,"参数%s不存在"),
    TAOBAOID_VIN_IS_BIND(1008,"该淘宝id和vin已绑定,请勿重复提交!"),
    VEHICLE_INFO_IS_NULL(1009,"该vin对应的车辆信息未找到,暂无法绑定!"),
    SEND_TO_CAR_EXCEPTION(1010, "SEND TO CAR FAILED!"),
    DATA_NOT_FOUND_EXCEPTION(1011, "%s数据不存在!"),
    CHECK_NAME_ALREADY_EXIST(1012, "名称重复"),
    DELETE_DATA_ERRAR(1013, "删除记录失败"),
    CREATE_ERRAR(1014, "创建记录失败"),
    UPDATE_DATA_ERRAR(1015, "更新数据失败"),
    GET_CURRENT_USER_ERROR(1016, "获取当前登录用户信息失败"),
    DELETE_DATA_ERRAR_COMPONY(1017, "该车厂被品牌引用，删除失败"),
    DELETE_DATA_ERRAR_BRAND(1018, "该品牌被车系引用，删除失败"),
    DELETE_DATA_ERRAR_SERIES(1019, "该车系被年款引用，删除失败"),
    DELETE_DATA_ERRAR_YEAR(1020, "该车年款被版本引用，删除失败"),
    RESPONSE_DATA(1021,"查询结果为空"),
    NOT_CONSTANTCLASS(1022,"Constant类%s不存在"),
    FILE_NOT_EXIST(1023,"文件不存在或已被删除"),
    
    SESSION_NOT_EXIST(1024,"Session不存在或已被删除"),
    SESSION_EXPIRED(1025,"Session已经过期"),
    
    PARAM_INVALID(1026, "参数非法：%s"),
    
    DELETE_DATA_NOT_ALLOW(1027, "数据不允许删除。原因：%s"),
    DELETE_DATA_NOT_EXIST(1028, "要删除的记录不存在。详细：{数据：%s, 条件:%s}"),
    
    RELATED_CITY_NOT_EXIST_IN_UPLOAD_CITY_LIST(1029, "要设置关联的城市不在上传的城市数据包列表里。相关数据：%s"),
    
    MU_USER_NOT_IN_WHITELIST(2001, "非法用户。该用户不在地图更新的白名单列表。%s"),
    MU_PARAM_INVALID_LOCALCITYVERSIONS(2002, "用户本地城市版本参数不正确。%s")
    ;

    ExceptionCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private int    code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 根据异常码获取异常
     * 
     * @param code 异常code
     * @return
     */
    public static ExceptionCodeEnum get(int code) {
        for (ExceptionCodeEnum exceptionEnum : ExceptionCodeEnum.values()) {
            if (exceptionEnum.getCode() == code) {
                return exceptionEnum;
            }
        }
        return null;
    }

    /**
     * 根据异常码获取异常信息
     * 
     * @param code 异常code
     * @return
     */
    public static String getMessageByCode(int code) {
        for (ExceptionCodeEnum exceptionEnum : ExceptionCodeEnum.values()) {
            if (exceptionEnum.getCode() == code) {
                return exceptionEnum.getMessage();
            }
        }
        return null;
    }
}
