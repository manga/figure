package com.figure.web.shiro.filter;

import com.figure.service.provide.security.ISecurityService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


public class SysUserFilter extends PathMatchingFilter {

	@Autowired
	private ISecurityService securityService;

    @Override
    protected boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

        String username = (String)SecurityUtils.getSubject().getPrincipal();
//        UserFormMap userFormMap = new UserFormMap();
//		userFormMap.put("accountName", "" + username + "");
        request.setAttribute("user", securityService.findAccountByNames(username));
        return true;
    }
}