package com.figure.web.util;


public class APIResult extends BaseAPIResult<Object, APIResult> {
	/**
	 * 
	 */
	public APIResult() {
		
	}
	public APIResult(int code, String msg, Object data){
		setStatus(new Status(code, msg));
		setData(data);
	}
}

