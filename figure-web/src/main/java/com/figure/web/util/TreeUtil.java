package com.figure.web.util;


import com.figure.service.dto.security.ResourcesDto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 把一个list集合,里面的bean含有 parentId 转为树形式
 *
 */
public class TreeUtil {
	
	
	/**
	 * 根据父节点的ID获取所有子节点
	 * @param list 分类表
	 * @param praentId 传入的父节点ID
	 * @return String
	 */
	public List<ResourcesDto> getChildTreeObjects(List<ResourcesDto> list,int praentId) {
		if(null==list || list.size()<=0)
			return  null;
		List<ResourcesDto> returnList = new ArrayList<ResourcesDto>();
		for (Iterator<ResourcesDto> iterator = list.iterator(); iterator.hasNext();) {
			ResourcesDto t = (ResourcesDto) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentId()==praentId) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}
	
	/**
	 * 递归列表
	 * @author lanyuan
	 * Email: mmm333zzz520@163.com
	 * @date 2013-12-4 下午7:27:30
	 * @param list
	 * @param t
	 */
	private void  recursionFn(List<ResourcesDto> list, ResourcesDto t) {
		List<ResourcesDto> childList = getChildList(list, t);// 得到子节点列表
		t.setChildren(childList);
		for (ResourcesDto tChild : childList) {
			if (hasChild(list, tChild)) {// 判断是否有子节点
				//returnList.add(ResourcesDto);
				Iterator<ResourcesDto> it = childList.iterator();
				while (it.hasNext()) {
					ResourcesDto n = (ResourcesDto) it.next();
					recursionFn(list, n);
				}
			}
		}
	}
	
	// 得到子节点列表
	private List<ResourcesDto> getChildList(List<ResourcesDto> list, ResourcesDto t) {
		
		List<ResourcesDto> tlist = new ArrayList<ResourcesDto>();
		Iterator<ResourcesDto> it = list.iterator();
		while (it.hasNext()) {
			ResourcesDto n = (ResourcesDto) it.next();
			if (n.getParentId() == t.getId()) {
				tlist.add(n);
			}
		}
		return tlist;
	} 
	List<ResourcesDto> returnList = new ArrayList<ResourcesDto>();
	/**
     * 根据父节点的ID获取所有子节点
     * @param list 分类表
     * @param typeId 传入的父节点ID
     * @param prefix 子节点前缀
     */
    public List<ResourcesDto> getChildTreeObjects(List<ResourcesDto> list, int typeId,String prefix){
        if(list == null) return null;
        for (Iterator<ResourcesDto> iterator = list.iterator(); iterator.hasNext();) {
            ResourcesDto node = (ResourcesDto) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (node.getParentId() == typeId) {
                recursionFn(list, node,prefix);
            }
            // 二、遍历所有的父节点下的所有子节点
            /*if (node.getParentId()==0) {
                recursionFn(list, node);
            }*/
        }
        return returnList;
    }
     
    private void recursionFn(List<ResourcesDto> list, ResourcesDto node,String p) {
        List<ResourcesDto> childList = getChildList(list, node);// 得到子节点列表
        if (hasChild(list, node)) {// 判断是否有子节点
            returnList.add(node);
            Iterator<ResourcesDto> it = childList.iterator();
            while (it.hasNext()) {
                ResourcesDto n = (ResourcesDto) it.next();
                n.setName(p+n.getName());
                recursionFn(list, n,p+p);
            }
        } else {
            returnList.add(node);
        }
    }

	// 判断是否有子节点
	private boolean hasChild(List<ResourcesDto> list, ResourcesDto t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
	
	// 本地模拟数据测试
	public void main(String[] args) {
		/*long start = System.currentTimeMillis();
		List<ResourcesDto> TreeObjectList = new ArrayList<ResourcesDto>();
		
		TreeObjectUtil mt = new TreeObjectUtil();
		List<ResourcesDto> ns=mt.getChildTreeObjects(TreeObjectList,0);
		for (ResourcesDto m : ns) {
			System.out.println(m.getName());
			System.out.println(m.getChildren());
		}
		long end = System.currentTimeMillis();
		System.out.println("用时:" + (end - start) + "ms");*/
	}
	
}
