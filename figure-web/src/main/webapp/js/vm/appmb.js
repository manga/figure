'use strict';

var page4 = angular.module('page4', ['ui.bootstrap', 'ngRoute']);

//page4.value('glparam',{"isLogin":false});

page4.config(['$routeProvider', '$locationProvider', '$sceProvider', function ($routeProvider, $locationProvider, $sceProvider) {
    $routeProvider
        .when('/about', {
            //controller: 'DoLoginCtrl',
            templateUrl: 'sps-web/about.html',
            publicAccess: true
        })
        .when('/', {
            controller: 'DoLoginCtrl',
            templateUrl: 'sps-web/html/security/login.html',
            publicAccess: true
        })
        .when('/index', {
            controller: 'NavbarCtrl',
            templateUrl: 'sps-web/about.html',
            publicAccess: true
        })
        .when('/role', {
            controller: 'customersCtrl',
            templateUrl: 'sps-web/html/security/roleList.html',
            publicAccess: true
        })
        .when('/addAccount', {
            controller: 'addAccountCtrl',
            templateUrl: 'sps-web/html/security/addAccount.html',
            publicAccess: true
        })
        .otherwise({redirectTo: '/'});
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);

page4.controller('NavbarCtrl', function ($scope,$rootScope,$http,$location) {
    $scope.isLogin = false;

    $scope.submit = function() {
        var pData = {username:$scope.username,password:$scope.password};
        $http({
            url:'sps-web/services/login.json',
            method: 'POST',
            params: pData
        }).success(function(result){
            if(result.success){
                //alert(result.success);
                $scope.isLogin = true;
                $location.path('/about');
            }else{
                $location.path('/');
                alert(result.msg);
            }
            //console.log("success!");
            //window.location.href = "html/index.html";
        }).error(function(){
            $location.path('/');
        })
    };

    $scope.loginOut = function() {
        var url = "sps-web/services/logout.json";
        $http.get(url).success(function (result) {
            $location.path('/');
            $scope.isLogin=false;
            //window.location.href = "../login.html";
            //$scope.doOpenMsg = result.APIResult.status.msg;//console.log();//$.alert({ body: result.APIResult.status.msg, height: 150 });
        });
    };
});

page4.controller('customersCtrl', function($scope, $rootScope, $http,$location) {
    $scope.selects = [
        {name : "是", value : "0"},
        {name : "否", value : "1"}
    ];
    $scope.queryBtn = function() {
        var pData = {name:$scope.name,status:$scope.status};
        $http({
            url:'sps-web/services/roleList.json',
            method: 'POST',
            params: pData
        }).success(function(result){
            if(result.success){
                $scope.items = result.data.resultList;
            }else{
                alert(result.msg);
            }
        }).error(function(){
            console.log("error");
        })
    };
//        $http.get("/try/angularjs/data/Customers_JSON.php")
//                .success(function (response) {$scope.items = response.records;});
});

page4.controller('addAccountCtrl', function($scope, $rootScope, $http,$location) {
    $scope.submit = function() {
        var pData = {userName:$scope.userName,accountName:$scope.accountName,password:$scope.password,description:$scope.description};
        $http({
            url:'sps-web/services/addAccount.json',
            method: 'POST',
            params: pData
        }).success(function(result){
            if(result.success){
                //alert(result.success);
                $location.path('/about');
            }else{
                $location.path('/');
                alert(result.msg);
            }
            //console.log("success!");
            //window.location.href = "html/index.html";
        }).error(function(){
            $location.path('/');
        })
    };
});

