'use strict';
var loginApp = angular.module('loginApp', []);
loginApp.controller('DoLoginCtrl', function($scope,$http) {
    $scope.submit = function() {
        var pData = {username:$scope.username,password:$scope.password};
        $http({
            url:'services/login.json',
            method: 'POST',
            params: pData
        }).success(function(){
            //console.log("success!");
            window.location.href = "html/index.html";
        }).error(function(){
            console.log("error");
        })
    };
});